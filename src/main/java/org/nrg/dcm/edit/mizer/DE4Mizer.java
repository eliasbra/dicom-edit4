package org.nrg.dcm.edit.mizer;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import org.nrg.dicomtools.exceptions.AttributeException;
import org.nrg.dcm.edit.ScriptApplicator;
import org.nrg.dicom.mizer.exceptions.MizerContextException;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.service.VersionString;
import org.nrg.dicom.mizer.service.impl.AbstractMizer;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.dicom.mizer.variables.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sun.util.resources.cldr.kk.CalendarData_kk_Cyrl_KZ;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.*;

/**
 * Handle anonymization with DicomEdit v4 scripts.
 */
@Component
public class DE4Mizer extends AbstractMizer {
    private static final Logger              _log              = LoggerFactory.getLogger(DE4Mizer.class);
    private static final List<VersionString> supportedVersions = Collections.singletonList(new VersionString("4.0"));
    private Map<MizerContextWithScript, ScriptApplicator> _contextMap;

    public DE4Mizer() {
        super(supportedVersions);
        _contextMap = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void aggregate(final MizerContext context, final Set<Variable> variables) throws MizerContextException {
        getReferencedVariables(context, variables);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Integer> getScriptTags(final List<MizerContext> contexts) {
        return new HashSet<Integer>() {{
            for (final MizerContext context : contexts) {
                addAll(getScriptTags(context));
            }
        }};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Integer> getScriptTags(final MizerContext context) {
        if (context instanceof MizerContextWithScript) {
            final MizerContextWithScript scriptContext = (MizerContextWithScript) context;

            try (final InputStream is = scriptContext.getScriptInputStream()) {
                final ScriptApplicator sa = new ScriptApplicator(is, context.getElements());
                return Sets.newHashSet(Collections2.transform(sa.getScriptTags(), new Function<Long, Integer>() {
                    @Nullable
                    @Override
                    public Integer apply(@Nullable final Long input) {
                        return input != null ? input.intValue() : null;
                    }
                }));
            } catch (IOException e) {
                _log.info("An error occurred reading or creating the submitted script object", e);
            } catch (ScriptEvaluationException e) {
                _log.info("There was an error in the submitted script", e);
            }
        }
        return Collections.emptySet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getReferencedVariables(final MizerContext context, final Set<Variable> variables) throws MizerContextException {
        if (!(context instanceof MizerContextWithScript)) {
            return;
        }

        final MizerContextWithScript scriptContext = (MizerContextWithScript) context;

        try (final InputStream input = scriptContext.getScriptInputStream()) {
            final ScriptApplicator applicator = new ScriptApplicator(input, context.getElements());
            for (final Variable variable : variables) {
                applicator.unify(variable);
            }
            variables.addAll(applicator.getVariables().values());
            context.setElement("variables", new LinkedHashSet<>(variables));
        } catch (IOException e) {
            _log.info("An error occurred reading or creating the submitted script object", e);
        } catch (ScriptEvaluationException e) {
            _log.info("There was an error in the submitted script", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void anonymizeImpl(final DicomObjectI dicomObject, final MizerContextWithScript context) throws MizerException {
        try {
            final ScriptApplicator applicator = getScriptApplicator( context);

            //noinspection unchecked
            final Set<Variable> unified = (Set<Variable>) context.getElement("variables");
            if (unified != null) {
                for (final Variable variable : unified) {
                    applicator.unify(variable);
                }
            }
            final Map<String, Variable> variables = applicator.getVariables();
            for (final String name : variables.keySet()) {
                final Variable variable = variables.get(name);
                if (variable.getInitialValue() == null) {
                    final String value = (String) context.getElement(name);
                    if (value == null) {
                        throw new MizerException(MessageFormat.format("Missing value for script-required variable: {0}", name));
                    }
                    variable.setValue(value);
                }
            }

            applicator.apply(null, dicomObject);
        } catch (ScriptEvaluationException | AttributeException e) {
            throw new MizerException(e);
        }
    }

    private ScriptApplicator createScriptApplicator( MizerContextWithScript context) throws MizerException {
        try (InputStream is = context.getScriptInputStream()) {
            return new ScriptApplicator( is, context.getElements());
        } catch (IOException | ScriptEvaluationException e) {
            throw new MizerException(e);
        }
    }

    @Override
    public void setContext( MizerContextWithScript context) throws MizerException {
        if( ! _contextMap.containsKey( context)) {
            _contextMap.put(context, createScriptApplicator(context));
        }
    }

    @Override
    public void removeContext( MizerContextWithScript context) {
        _contextMap.remove( context);
    }

    private ScriptApplicator getScriptApplicator( MizerContextWithScript context) throws MizerException {
        return (_contextMap.containsKey( context))? _contextMap.get( context): createScriptApplicator( context);
    }

    @Override
    protected String getMeaning() {
        return "XNAT DicomEdit 4 Script";
    }

    @Override
    protected String getSchemeDesignator() {
        return "XNAT";
    }

    @Override
    protected String getSchemeVersion() {
        return "1.0";
    }
}
